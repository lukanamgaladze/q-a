package com.example.qa.fragments

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.example.qa.R

class FinalFragment : Fragment(R.layout.fragment_final){

    lateinit var finish1 : TextView
    lateinit var finish2 : TextView
    lateinit var finish3 : TextView

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        finish1 = view.findViewById(R.id.finish1)
        finish2 = view.findViewById(R.id.finish2)
        finish3 = view.findViewById(R.id.finish3)

        finish1.text = FinalFragmentArgs.fromBundle(requireArguments()).number.toString()
        finish2.text = FinalFragmentArgs.fromBundle(requireArguments()).number2.toString()
        finish3.text = FinalFragmentArgs.fromBundle(requireArguments()).number3.toString()

    }

}

